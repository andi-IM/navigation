package my.id.airham.navigationandi;

import static com.android.volley.Request.Method.GET;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import my.id.airham.navigationandi.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    static final String URL = "https://tokonya-andi.000webhostapp.com/show_marker.php";
    private static final float MAP_ZOOM = 12f;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private ArrayList<LatLng> latLngList = new ArrayList<>();
    private MarkerOptions marker = new MarkerOptions();
    private JSONArray result;
    private ActivityMapsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        latLngList.add(new LatLng(-0.9145203275488303, 100.46615192078089));
        latLngList.add(new LatLng(-0.9150370675779327, 100.45792423969134));
        latLngList.add(new LatLng(-0.8972307590164963, 100.35077568387176));
        latLngList.add(new LatLng(-0.9585553259698995, 100.36366939583554));
        latLngList.add(new LatLng(-0.956956627371106, 100.3636978685277));

        mMap.addMarker(new MarkerOptions().position(latLngList.get(0)).title("Politeknik Negeri Padang").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
        mMap.addMarker(new MarkerOptions().position(latLngList.get(1)).title("Universitas Andalas").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.addMarker(new MarkerOptions().position(latLngList.get(2)).title("Universitas Negeri Padang").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        mMap.addMarker(new MarkerOptions().position(latLngList.get(3)).title("J-BROS").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        mMap.addMarker(new MarkerOptions().position(latLngList.get(4)).title("Max-Indo").icon(BitmapDescriptorFactory.fromResource(R.drawable.common_full_open_on_phone)));

        // Add a marker in Sydney and move the camera
        LatLng basko = new LatLng(-0.9022796529642999, 100.3508266366157);
        mMap.addMarker(new MarkerOptions().position(basko).title("Tando di Basko, Sanak!"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(basko, MAP_ZOOM));

        enableMyLocation();
        enableLongClick(mMap);
        enableMapStyles(mMap);
        enableDynamicMarker();
    }

    private void enableMyLocation() { }

    private void enableLongClick(GoogleMap mMap) {
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(@NonNull LatLng latLng) {
                String snippet = String.format(Locale.getDefault(),
                        getString(R.string.lat_long_snippet),
                        latLng.latitude,
                        latLng.longitude
                );
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(getString(R.string.droppin))
                        .snippet(snippet)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_two))
                );
            }
        });
    }

    private void enableDynamicMarker() {
        // buat antrian menggunakan method newRequestQueue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        // memanggil data dari network menggunakan HTTP header GET
        StringRequest stringRequest = new StringRequest(GET, URL,
                // jika berhasil mendapatkan data (HTTP.statusCode == 200)
                (Response.Listener<String>) response -> {
                    Log.d(TAG, "enableDynamicMarker: " + response);
                    JSONObject object = null;

                    try {
                        object = new JSONObject(response);
                        result = object.getJSONArray("LOCATION");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject obj = object = result.getJSONObject(i);
                            String latPoint = obj.getString("1");
                            String longPoint = obj.getString("2");
                            String locationName = obj.getString("3");
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(latPoint), Double.parseDouble(longPoint)))
                                    .title(Double.valueOf(latPoint).toString() + "," + Double.valueOf(longPoint).toString())
                                    .icon(BitmapDescriptorFactory.defaultMarker
                                            (BitmapDescriptorFactory.HUE_MAGENTA))
                                    .snippet(locationName));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom
                                    (new LatLng(-0.9022796529642999, 100.3508266366157),
                                            13.0f));
                        }
                    }
                    // menangkap error jika terdapat kesalahan parsing JSON
                    catch (NullPointerException | JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            error.printStackTrace();
            Toast.makeText(MapsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
        });
        int socketTimeout = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void enableMapStyles(GoogleMap googleMap) {
        try {
            boolean mapStyle = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_styles));

            if (!mapStyle) {
                Log.d(TAG, "enableMapStyle: Cannot find new mapStyle");
            }

        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "enableMapStyle: Cannot load RawResource Style" + e.getMessage());
        }
    }
}